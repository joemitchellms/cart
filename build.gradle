plugins {
    id 'org.springframework.boot' version '2.4.2'
    id 'io.spring.dependency-management' version '1.0.11.RELEASE'
    id 'java'
    id 'maven-publish'
}

group = 'shop.velox'
sourceCompatibility = '11'

repositories {
    mavenCentral()
    maven {
        url "https://gitlab.com/api/v4/groups/velox-shop/-/packages/maven"
        name "GitLab"
    }
}

task publicJar(type: Jar, dependsOn: compileJava) {
    archiveName "${project.name}-${project.version}-api.jar"
    includeEmptyDirs = false
    from sourceSets.main.output
    include 'shop/velox/cart/api/dto/*'
}

dependencies {
    implementation('shop.velox:velox-starter:3.0.+')
    compile('org.springframework.boot:spring-boot-starter')
    compile('org.springframework.boot:spring-boot-starter-cache')
    compile('org.springframework.boot:spring-boot-starter-data-jpa')
    compile('org.springframework.boot:spring-boot-starter-jersey')
    compile('org.springframework.boot:spring-boot-starter-security')
    compile('org.springframework.boot:spring-boot-starter-validation')
    compile('org.springframework.boot:spring-boot-starter-web')
    compile('org.springframework.boot:spring-boot-starter-webflux')
    compile('org.springframework.session:spring-session-core')
    compile('org.springframework.boot:spring-boot-starter-actuator')
    compile('org.apache.commons:commons-lang3')
    compileOnly('org.springframework.boot:spring-boot-configuration-processor')

    compile group: 'ma.glasnost.orika', name: 'orika-core', version: '1.5.4'

    runtime('org.springframework:spring-aspects')
    compile group: 'org.springframework.retry', name: 'spring-retry', version: '1.2.5.RELEASE'

    compile group: 'org.springdoc', name: 'springdoc-openapi-ui', version: '1.3.2'
    compile group: 'org.springdoc', name: 'springdoc-openapi-groovy', version: '1.3.2'
    compile group: 'org.springdoc', name: 'springdoc-openapi-data-rest', version: '1.3.2'


    runtime('mysql:mysql-connector-java')
    runtime('org.apache.derby:derby')

    testCompile('io.projectreactor:reactor-test')
    testCompile('org.springframework.security:spring-security-test')
    testCompile('org.springframework.boot:spring-boot-starter-test') {
        exclude group: 'org.junit.vintage', module: 'junit-vintage-engine'
    }
}

test {
    useJUnitPlatform()
    testLogging.showStandardStreams = true
    testLogging.exceptionFormat = 'full'
}

def springProfile = 'default'
bootRun {
    bootRun.systemProperty 'spring.profiles.active', "${springProfile}"
}

publishing {
    publications {
        pluginMavenDevelop(MavenPublication) {
            groupId = "${project.group}"
            artifactId = "${project.name}" + "-api"
            version = "${project.version}"
            artifact "build/libs/" + "${project.name}-" + "${project.version}" + "-api.jar"
        }
    }
    repositories {
        maven {
            url "https://gitlab.com/api/v4/projects/17879967/packages/maven"
            credentials(HttpHeaderCredentials) {
                name = 'Job-Token'
                value = System.getenv('CI_JOB_TOKEN')
            }
            authentication {
                header(HttpHeaderAuthentication)
            }
        }
    }
}

task(getVersion) {
    println(version)
    return
}


task publishDocker {
    doLast {
        String minorVersion = "${project.version}".substring(0, "${project.version}".lastIndexOf("."))
        exec {
            commandLine "docker", "login", "-u", registryUser, "-p", registryPassword, registry
        }
        exec {
            commandLine "docker", "build", "--pull", "-t" + registryImage,
                    "-t" + (registryImage + ":" + version), "-t" + (registryImage + ":" + minorVersion), "."
        }
        exec {
            commandLine "docker", "push", "--all-tags", registryImage
        }
    }
}
