package shop.velox.cart.dao;


import java.util.Optional;
import shop.velox.cart.model.ItemEntity;

public interface ItemRepository extends CustomRepository<ItemEntity, Long> {

  Optional<ItemEntity> findItemByCartIdAndId(String cartId, String itemId);

  Optional<ItemEntity> findItemByCartIdAndArticleId(String cartId, String articleId);

}
