package shop.velox.cart.dao;

import java.util.List;
import java.util.Optional;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import shop.velox.cart.model.CartEntity;

public interface CartRepository extends CustomRepository<CartEntity, Long> {

  Optional<CartEntity> findById(String id);

  Optional<CartEntity> findByIdAndUserId(String id, String userId);

  Long deleteCartById(String id);

  List<CartEntity> findByUserId(String userId);

  Page<CartEntity> findAll(Pageable pageable);

}
