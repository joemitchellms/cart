package shop.velox.cart;

import org.springdoc.core.SpringDocUtils;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import shop.velox.cart.dao.impl.CustomRepositoryImpl;

@Configuration
@EnableJpaRepositories(basePackages = {
    "shop.velox.cart.dao"}, repositoryBaseClass = CustomRepositoryImpl.class)
@EnableJpaAuditing
@EnableTransactionManagement
public class AppConfig {

  static {
    SpringDocUtils.getConfig().addResponseWrapperToIgnore(Mono.class);
    SpringDocUtils.getConfig().addResponseWrapperToIgnore(Flux.class);
  }

}