package shop.velox.cart.api.controller.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Mono;
import shop.velox.cart.api.controller.ItemController;
import shop.velox.cart.api.dto.CartDto;
import shop.velox.cart.api.dto.ItemDto;
import shop.velox.cart.model.CartEntity;
import shop.velox.cart.model.ItemEntity;
import shop.velox.cart.service.CartService;
import shop.velox.commons.converter.Converter;


@RestController
public class ItemControllerImpl implements ItemController {

  private static final Logger LOG = LoggerFactory.getLogger(ItemControllerImpl.class);

  private final CartService cartService;

  private final Converter<CartEntity, CartDto> cartConverter;
  private final Converter<ItemEntity, ItemDto> itemConverter;

  public ItemControllerImpl(final @Autowired CartService cartService,
      final @Autowired Converter<CartEntity, CartDto> cartConverter,
      final @Autowired Converter<ItemEntity, ItemDto> itemConverter) {
    this.cartService = cartService;
    this.cartConverter = cartConverter;
    this.itemConverter = itemConverter;
  }

  @Override
  public Mono<ResponseEntity<ItemDto>> getItem(String cartId, String itemId) {
    return Mono.fromSupplier(() -> getCartService().getItem(cartId, itemId)
        .map(itemEntity -> new ResponseEntity<>(getItemConverter().convertEntityToDto(itemEntity),
            null, HttpStatus.OK))
        .orElseGet(() -> new ResponseEntity<>(HttpStatus.NOT_FOUND)));
  }

  @Override
  public Mono<ResponseEntity<CartDto>> addItem(String cartId, ItemDto item) {
    return Mono.fromSupplier(
        () -> getCartService().addItem(cartId, getItemConverter().convertDtoToEntity(item)));
  }

  @Override
  public Mono<ResponseEntity<CartDto>> updateItem(String id, ItemDto item) {
    return Mono.fromSupplier(() -> getCartService().updateItem(id, item));
  }

  @Override
  public Mono<ResponseEntity<CartDto>> removeItem(String id, String itemId) {
    return Mono.fromSupplier(() -> getCartService().removeItem(id, itemId));
  }

  protected CartService getCartService() {
    return cartService;
  }

  protected Converter<CartEntity, CartDto> getCartConverter() {
    return cartConverter;
  }

  protected Converter<ItemEntity, ItemDto> getItemConverter() {
    return itemConverter;
  }
}
