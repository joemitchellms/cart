package shop.velox.cart.api.dto;

import io.swagger.v3.oas.annotations.media.Schema;
import java.math.BigDecimal;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

public class ItemDto {

  @Schema(description = "Unique identifier of the Item.", example = "2e5c9ba8-956e-476b-816a-49ee128a40c9")
  private final String id;

  @Schema(description = "Unique identifier of the Article.", example = "article1", required = true)
  private final String articleId;

  @Schema(description = "Quantity of article in this Item.", example = "4", required = true)
  private final BigDecimal quantity;

  @Schema(description = "Name of the Article.", example = "Pencil")
  private String name;

  @Schema(description = "Unit price of the Article.", example = "1050.25")
  private BigDecimal unitPrice;

  @Schema(description = "Total price of the Article for required quantity.", example = "4201")
  private BigDecimal totalPrice;


  public ItemDto(String id, String articleId, BigDecimal quantity, String name, BigDecimal unitPrice, BigDecimal totalPrice) {

    this.id = id;
    this.articleId = articleId;
    this.quantity = quantity;
    this.name = name;
    this.unitPrice = unitPrice;
    this.totalPrice = totalPrice;
  }

  public String getId() {
    return id;
  }

  public String getArticleId() {
    return articleId;
  }

  public BigDecimal getQuantity() {
    return quantity;
  }

  public String getName() { return name; }

  public void setName(String name) { this.name = name; }

  public BigDecimal getUnitPrice() { return unitPrice; }

  public void setUnitPrice(BigDecimal unitPrice) { this.unitPrice = unitPrice; }

  public BigDecimal getTotalPrice() { return totalPrice; }

  public void setTotalPrice(BigDecimal totalPrice) { this.totalPrice = totalPrice; }

  @Override
  public String toString() {
    return ToStringBuilder.reflectionToString(this, ToStringStyle.JSON_STYLE);
  }

}
