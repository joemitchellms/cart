package shop.velox.cart.api.dto;

import io.swagger.v3.oas.annotations.media.Schema;
import java.util.ArrayList;
import java.util.List;
import javax.validation.constraints.NotNull;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

public class CartDto {

  @Schema(description = "Unique identifier of the Cart.", example = "4b7e6hjA-107b-476b-816a-51hh256a30d1")
  private final String id;

  @Schema(description = "Unique identifier of the User.", example = "4b7e6hjA-107b-476b-816a-51hh256a30d1")
  private  String userId;

  @Schema(description = "The ID of the currency of the cart.", example = "0d449436-2702-4051-8a85-e3ec5b645959")
  @NotNull
  private  String currencyId;

  @Schema(description = "List of the Items in the Cart.")
  private final List<ItemDto> items;

  public CartDto(String id, List<ItemDto> items) {
    this.id = id;
    this.items = items;
  }

  public CartDto(String id) {
    this(id, new ArrayList<>());
  }

  public String getId() {
    return id;
  }

  public List<ItemDto> getItems() {
    return items;
  }

  public String getUserId() {
    return userId;
  }

  public void setUserId(String userId) {
    this.userId = userId;
  }

  public String getCurrencyId() {
    return currencyId;
  }

  public void setCurrencyId(String currencyId) {
    this.currencyId = currencyId;
  }

  @Override
  public String toString() {
    return ToStringBuilder.reflectionToString(this, ToStringStyle.JSON_STYLE);
  }
}
