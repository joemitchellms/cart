package shop.velox.cart.converter.impl;

import ma.glasnost.orika.MapperFacade;
import ma.glasnost.orika.MapperFactory;
import ma.glasnost.orika.impl.DefaultMapperFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import shop.velox.cart.api.dto.CartDto;
import shop.velox.cart.model.CartEntity;
import shop.velox.commons.converter.Converter;

@Component
public class CartConverterImpl implements Converter<CartEntity, CartDto> {

  private static final Logger LOG = LoggerFactory.getLogger(ItemConverterImpl.class);
  private final MapperFacade mapperFacade;

  public CartConverterImpl() {
    MapperFactory mapperFactory = new DefaultMapperFactory.Builder().build();
    mapperFactory
        .classMap(CartEntity.class, CartDto.class)
        .byDefault()
        .register();

    mapperFacade = mapperFactory.getMapperFacade();
  }

  @Override
  public CartDto convertEntityToDto(CartEntity cartEntity) {
    CartDto cartDto = getMapperFacade().map(cartEntity, CartDto.class);
    LOG.debug("Converted {} to {}", cartEntity, cartDto);
    return cartDto;
  }

  @Override
  public CartEntity convertDtoToEntity(CartDto cartDto) {
    CartEntity cartEntity = getMapperFacade().map(cartDto, CartEntity.class);
    LOG.debug("Converted {} to {}", cartDto, cartEntity);
    return cartEntity;
  }

  public MapperFacade getMapperFacade() {
    return mapperFacade;
  }


}
