package shop.velox.cart.converter.impl;

import ma.glasnost.orika.MapperFacade;
import ma.glasnost.orika.MapperFactory;
import ma.glasnost.orika.impl.DefaultMapperFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import shop.velox.cart.api.dto.ItemDto;
import shop.velox.cart.model.ItemEntity;
import shop.velox.commons.converter.Converter;

@Component
public class ItemConverterImpl implements Converter<ItemEntity, ItemDto> {

  private static final Logger LOG = LoggerFactory.getLogger(ItemConverterImpl.class);
  private final MapperFacade mapperFacade;

  public ItemConverterImpl() {
    MapperFactory mapperFactory = new DefaultMapperFactory.Builder().build();
    mapperFactory
        .classMap(ItemEntity.class, ItemDto.class)
        .byDefault()
        .register();

    mapperFacade = mapperFactory.getMapperFacade();
  }

  @Override
  public ItemDto convertEntityToDto(final ItemEntity entity) {
    ItemDto dto = getMapperFacade().map(entity, ItemDto.class);
    LOG.debug("Converted {} to {}", entity, dto);
    return dto;
  }

  @Override
  public ItemEntity convertDtoToEntity(final ItemDto dto) {
    ItemEntity entity = getMapperFacade().map(dto, ItemEntity.class);
    LOG.debug("Converted {} to {}", dto, entity);
    return entity;
  }

  public MapperFacade getMapperFacade() {
    return mapperFacade;
  }
}
