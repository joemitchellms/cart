package shop.velox.cart.model;

import java.math.BigDecimal;
import java.util.UUID;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringExclude;
import org.apache.commons.lang3.builder.ToStringStyle;
import shop.velox.commons.model.AbstractEntity;


@Entity
@Table(name = "ITEMS")
public class ItemEntity extends AbstractEntity {

  private static final long serialVersionUID = -1230190190208576850L;

  @Column(name = "ID", nullable = false, unique = true)
  private final String id;

  @Column(name = "ARTICLE_ID", nullable = false)
  private String articleId;

  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = "CART_PK")
  @ToStringExclude
  private CartEntity cart;

  @Column(name = "QUANTITY", nullable = false)
  private BigDecimal quantity;

  @Column(name = "NAME")
  private String name;

  @Column(name = "UNIT_PRICE")
  private BigDecimal unitPrice;

  @Column(name = "TOTAL_PRICE")
  private BigDecimal totalPrice;

  public ItemEntity() {
    id = UUID.randomUUID().toString();
  }

  public ItemEntity(final String articleId, final CartEntity cart, final BigDecimal quantity,
      String name, BigDecimal unitPrice, BigDecimal totalPrice) {
    this();
    setArticleId(articleId);
    setCart(cart);
    setQuantity(quantity);
    setName(name);
    setUnitPrice(unitPrice);
    setTotalPrice(totalPrice);
  }

  public ItemEntity(final String articleId, final CartEntity cart, final BigDecimal quantity) {
    this(articleId, cart, quantity, null, null, null);
  }

  public String getId() {
    return id;
  }

  public String getArticleId() {
    return articleId;
  }

  public void setArticleId(final String articleId) {
    this.articleId = articleId;
  }

  public CartEntity getCart() {
    return cart;
  }

  public void setCart(final CartEntity cart) {
    this.cart = cart;
    cart.addItem(this);
  }

  public BigDecimal getQuantity() {
    return quantity;
  }

  public void setQuantity(final BigDecimal quantity) {
    this.quantity = quantity;
  }

  public String getName() {
    return name;
  }

  public void setName(final String name) {
    this.name = name;
  }

  public BigDecimal getUnitPrice() {
    return unitPrice;
  }

  public void setUnitPrice(final BigDecimal unitPrice) {
    this.unitPrice = unitPrice;
  }

  public BigDecimal getTotalPrice() {
    return totalPrice;
  }

  public void setTotalPrice(final BigDecimal totalaPrice) {
    this.totalPrice = totalaPrice;
  }

  @Override
  public String toString() {
    return ToStringBuilder.reflectionToString(this, ToStringStyle.JSON_STYLE);
  }
}
