package shop.velox.cart;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Primary;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.provisioning.InMemoryUserDetailsManager;

@TestConfiguration
public class CartTestConfiguration {

  @Bean
  @Primary
  public UserDetailsService userDetailsService() {
    final List<UserDetails> users = new ArrayList<>();
    users.add(createNewUser("user@velox.shop", "password", "User"));
    users.add(createNewUser("admin@velox.shop", "password", "User", "Admin"));
    users.add(createNewUser("cart_admin@velox.shop", "password", "User", "Cart_Admin"));
    return new InMemoryUserDetailsManager(users);
  }

  private UserDetails createNewUser(final String username, final String password,
      final String... authorities) {
    return new User(username, password,
        Stream.of(authorities).map(SimpleGrantedAuthority::new).collect(Collectors.toList()));
  }
}
