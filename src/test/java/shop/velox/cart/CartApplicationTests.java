package shop.velox.cart;

import static org.junit.jupiter.api.Assertions.assertNotNull;

import javax.annotation.Resource;
import org.junit.jupiter.api.MethodOrderer.OrderAnnotation;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;

@SpringBootTest
@ContextConfiguration(classes = {shop.velox.cart.CartTestConfiguration.class})
@ActiveProfiles({"embeddeddb", "localauth"})
@TestMethodOrder(OrderAnnotation.class)
public class CartApplicationTests {

  @Resource
  protected ConfigurableApplicationContext appContext;
  @Autowired
  protected UserDetailsService userDetailsService;

  @Test
  public void test1ContextLoads() {
    assertNotNull(appContext, "Application context must not be null");
    final CartApplication app = appContext.getBean(CartApplication.class);
    assertNotNull(app, "Application Bean must not be null");
  }

}
